'''
Created on 01.11.2021

@author: Fernando Penaherrera @UOL/OFFIS

A mosaik adapter for the radiator simulator

'''
import mosaik_api
from src.scenarios.radiator.radiator import Radiator
import itertools

# Simulation Metadata
META = {
    'type': 'time-based',
    'models': {
        'Radiator': {
            'public': True,
            'params': ["model_params"],
            'attrs': [
                "t_supply",         # Radiator supply temperature [C]
                "t_return",         # Radiator return temperature [C]
                "t_indoor",         # Indoor temperature [C]
                "Q_demand",         # Heat Demand [W]
                "massflow_in",         # Radiator massflow  in [kg/s]
                "massflow_out",         # Radiator massflow  in [kg/s]
                "signal",    # Delete later
            ],
        },
    },
    "extra_methods": ["get_entities"]}

# Mosaik API


class RadiatorSimulator(mosaik_api.Simulator):
    '''
    Simulator to be coupled with the cosimulation framework
    '''

    def __init__(self):
        '''
        Initialization
        All methods in the inheritance chain are called

        '''

        super(RadiatorSimulator, self).__init__(META)

        self.sid = None
        self.entities = {}
        self._entities = {}
        self.eid_counters = {}

    def init(self, sid, time_resolution=1, step_size=60):
        '''
        Initialization of entities

        :param sid: String ID.
        :param time_resolution: Resolution in seconds
        :param step_size: Step size in seconds
        '''

        # assign properties
        self.sid = sid
        self.time_resolution = time_resolution
        self.step_size = step_size

        # return meta data
        return self.meta

    def create(self, num, model_type, model_params):
        '''

        :param num: Number of instances of the model_type
        :param model_type: Class specifiying the model_type. In this case, Radiator
        :param model_params: Dictionary for the initialization.
                        design_params = {
                                "t_supply_design": 60,
                                "t_return_design": 35,
                                "t_indoor_design": 22,
                                "method": "LMTD",
                                "Q_design": 5000,
                                "n_coeff": 1.3}
        '''
        # This is the Value to be returned in case PVSystem is not found.
        counter = self.eid_counters.setdefault(model_type, itertools.count())

        entities = []
        if model_params:
            self.model_params = model_params

        for _ in range(num):
            eid = '{}_{}'.format(model_type, next(counter))  # Entities IDs

            self._entities[eid] = Radiator(params=model_params)

            full_id = '{}.{}'.format(self.sid, model_type)
            entities.append({'eid': eid, 'type': model_type, 'rel': []})

            self.entities[eid] = {
                'ename': eid,
                'etype': model_type,
                'model': self._entities[eid],
                'full_id': full_id}

        return entities

    def step(self, time, inputs, max_advance=3600):
        '''
        Perform simulaation step

        :param time: Current simulation time
        :param inputs: Inputs as given by the simmulator
        :param max_advance: Max advance in seconds
        '''

        # loop over input signals
        for eid, attrs in inputs.items():
            for attr, values in attrs.items():
                # set value
                if attr == "t_supply":
                    self._entities[eid].t_supply = sum(values.values())
                if attr == "t_indoor":
                    self._entities[eid].t_indoor = sum(values.values())
                if attr == "Q_demand":
                    self._entities[eid].Q_demand = sum(values.values())
                if attr == "signal":
                    print("val ---", sum(values.values()))
                    self._entities[eid].signal = sum(values.values())

        # Perform simulation step
            self._entities[eid].step()

        # Next timestamp for simulation
        return time + self.step_size

    def get_data(self, outputs):
        '''
        Get data of the model after the step()

        :param outputs: Output parameters

        Desired output parameters:
        "t_return",         # Radiator return temperature [C]
        "massflow_in",      # Radiator massflow [kg/s]
        "massflow_out"     # Radiator massflow [kg/s]

        '''
        # create output data
        data = {}

        # loop over all output values
        for ename, attrs in outputs.items():
            # get model entry
            entry = self._entities[ename]

            # set data entry for model
            data[ename] = {}

            # loop over all attributes
            for attr in attrs:
                if attr not in self.meta['models']["Radiator"]['attrs']:
                    raise ValueError('Unknown output attribute: %s' % attr)

                # Get model data
                else:
                    data[ename][attr] = vars(entry)[attr]

        # return data to mosaik
        return data

    def get_entities(self):
        # return entities of API
        return self.entities
