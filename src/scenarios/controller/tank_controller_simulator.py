'''
Created on 09.11.2021

@author: Fernando Penaherrera @UOL/OFFIS
'''
import itertools
import mosaik_api

# Simulation Metadata

META = {
    'type': 'time-based',
    'models': {
        'TankController': {
            'public': True,
            'params': ["model_params", "controlled_entities"],
            'attrs': [
                "signal",            # Double value of the input signal for the heat rod
                "T_mean_tank",       # Mean temperature of the tank
                "T_upper_sensor",    # Temperature of the upper sensor
                "T_heatpump_sensor",  # Temperature of the sensor at the heat pump
                "P_th_set"           # Output signal for output of the heat rod
            ],
        },
    },
}


class TankControllerSimulator(mosaik_api.Simulator):
    '''
    Mock-Up simulator that sets the tank temperature.

    '''

    def __init__(self):
        '''
        Initialization
        All methods in the inheritance chain are called
        '''
        super(TankControllerSimulator, self).__init__(META)
        self.sid = None
        self.entities = {}
        self._entities = {}
        self.eid_counters = {}

    def init(self, sid, time_resolution=1, step_size=60):
        '''
        Initialization of entities

        :param sid: String ID.
        :param time_resolution: Resolution in seconds
        :param step_size: Step size in seconds
        '''

        # assign properties
        self.sid = sid
        self.time_resolution = time_resolution
        self.step_size = step_size

        # return meta data
        return self.meta

    def create(self, num, model_type, controlled_entities=None):
        '''

        :param num: Number of instances of the model_type
        :param model_type: Class specifiying the model_type. In this case, TankController
        :param controlled_entity: Tank Entity to be controlled
        '''

        counter = self.eid_counters.setdefault(model_type, itertools.count())

        tank_entities_dict = controlled_entities.sim._inst.models
        if len(tank_entities_dict.keys()) > 1:
            raise ValueError("Only one tank can be controlled")

        for _, models in tank_entities_dict.items():
            self.controlled_entities = models

        for k in self.controlled_entities.heating_rods.keys():
            # There is only one
            # "self.controlled_entities.heating_rods[k]" is the heating rod
            self.max_temperature = self.controlled_entities.heating_rods[k].T_max
            self.P_th_max = self.controlled_entities.heating_rods[k].P_th_max

        entities = []

        for _ in range(num):
            eid = '{}_{}'.format(model_type, next(counter))  # Entities IDs
            entities.append({'eid': eid, 'type': model_type, 'rel': []})

        return entities

    def step(self, time, inputs, max_advance=36000):
        '''
        Perform simulaation step

        :param time: Current simulation time
        :param inputs: Inputs as given by the simulator
        :param max_advance: Max advance in seconds
        '''
        # Perform simulation step
        self.P_th_set = None
        self.cache = {}

        for _, attrs in inputs.items():
            for attr, values in attrs.items():
                # set value
                if attr == "signal":
                    control_signal = sum(values.values())
                if attr == "T_upper_sensor":
                    T_upper_sensor = sum(values.values())
                if attr == "T_heatpump_sensor":
                    T_heatpump_sensor = sum(values.values())

        #======================================================================
        # Here goes the controller logic
        #======================================================================

        # Override max temperature information
        # Hint: A min temperature should also be encoded
        self.min_temp_heatpump_entry = 25

        # Turn off if the max temperature is 90�C
        if T_upper_sensor > self.max_temperature:
            P_th_set = 0
        # Force heating if tank is too cold
        else:
            if T_heatpump_sensor < self.min_temp_heatpump_entry:
                P_th_set = self.P_th_max
            else:
                if control_signal <= 0:
                    P_th_set = 0
                else:
                    if control_signal > self.P_th_max:
                        P_th_set = self.P_th_max
                    else:
                        P_th_set = control_signal


        self.P_th_set = P_th_set

        #=======================================================================
        # End of controler logic        
        #=======================================================================
        
        return time + self.step_size

    def get_data(self, outputs):
        '''
        This shall return none
        '''
        # create output data
        data = {}

        # loop over all output values
        for ename, attrs in outputs.items():
            # get model entry
            # set data entry for model
            data[ename] = {}

            # loop over all attributes
            for attr in attrs:
                if attr not in self.meta['models']["TankController"]['attrs']:
                    raise ValueError('Unknown output attribute: %s' % attr)

                # Get model data
                else:
                    if attr == "P_th_set":
                        data[ename][attr] = float(self.P_th_set)

        return data


if __name__ == "__main__":
    pass
