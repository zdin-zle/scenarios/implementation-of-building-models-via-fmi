'''
Created on 05.11.2021

@author: Fernando Penaherrera @UOL/OFFIS
'''
import arrow

class SignalGenerator(object):
    '''
    classdocs
    '''
    signals = None
    statusSim = 0  # Signal 
    DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss' # Date Time Format

    def __init__(self, start_date, signals):
        '''
        Constructor

        :param signals: A dictionary with date-time stamps for 
                        switching the signal value. Ex:
                            signals = {
                                    "2021-01-01 07:00:00": 1000,
                                    "2021-01-02 16:00:00": 1200,
                                }
        '''
        self.signals = signals
        
        # This one is used by the step function in MOSAIK
        self.date = arrow.get(start_date, self.DATE_FORMAT)

    def step(self, step_size):
        '''
        Advance the current time.

        :param step_size: Interval in seconds of the time step for co-simulations
        '''
        self.date = self.date.shift(seconds=step_size)
        current_date = self.date.format(self.DATE_FORMAT)
         
        if current_date in self.signals.keys():
            self.statusSim = int(self.signals[current_date])


if __name__ == "__main__":
    start_date = "2021-01-01 06:00:00"
    signals = {
        "2021-01-01 07:00:00": 1000,
        "2021-01-01 15:00:00": 2500,
        "2021-01-01 18:00:00": 3000,
        "2021-01-01 19:00:00": 666,
        "2021-01-01 20:00:00": 0,
    }

    signal_generator = SignalGenerator(start_date, signals)
    
    for _ in range (48):
        print(signal_generator.statusSim)
        signal_generator.step(step_size=30*60)
    