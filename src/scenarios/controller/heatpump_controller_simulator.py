'''
Created on 10.11.2021

@author: Fernando Penaherrera @UOL/OFFIS
'''

import itertools
import mosaik_api

'''
Connection through sensors to compute the set output power

Tank Temps. Signal ----> | Controller Logic | ----> Heatpump set demand
Heat Demand  Signal ---> |                  |

'''

# Simulation Metadata
META = {
    'type': 'time-based',
    'models': {
        'HeatpumpController': {
            'public': True,
            'params': ["model_params", "controlled_entities"],
            'attrs': [
                "Q_demand_signal",         # Signal of Q_demand
                "T_in_tank",               # Temperature input from the tank
                "T_mean_tank",   # Mean Temperature of the tank
                "Q_Demand_controlled"  # Controlled value of demand
            ],
        },
    },
}


class HeatpumpControllerSimulator(mosaik_api.Simulator):
    '''
    Simulator to control a heat pump object

    '''

    def __init__(self):
        '''
        Initialization
        All methods in the inheritance chain are called
        '''
        super(HeatpumpControllerSimulator, self).__init__(META)
        self.sid = None
        self.entities = {}
        self._entities = {}
        self.eid_counters = {}

    def init(self, sid, time_resolution=1, start_date=None, step_size=60):
        '''
        Initialization of entities

        :param sid: String ID.
        :param time_resolution: Resolution in seconds
        :param start_date: Start date. YYYY-MM-DDD hh:mm:ss
        :param step_size: Step size in seconds
        '''

        # assign properties
        self.sid = sid
        self.time_resolution = time_resolution
        self.step_size = step_size

        self.min_temperature = None     # Minimal temperature of heatpump entry
        self.min_tank_mean_T = None     # Desired minimal mean tank temperature
        self.Q_demand_min = None        # Minimum demand for the heatpump to operate
        # Maximal input temperature to avoid heatpump malfunctioning
        self.max_input_temperature = None

        # return meta data
        return self.meta

    def create(self, num, model_type, model_params=None):
        '''

        :param num: Number of instances of the model_type
        :param model_type: Class specifiying the model_type. In this case, TankController
        '''
        if num != 1:
            raise ValueError("Only one entity can be created")

        if model_params:
            for attr, val in model_params.items():
                setattr(self, attr, val)

        counter = self.eid_counters.setdefault(model_type, itertools.count())

        entities = []

        for _ in range(num):
            eid = '{}_{}'.format(model_type, next(counter))  # Entities IDs
            self._entities[eid] = None
            entities.append({'eid': eid, 'type': model_type, 'rel': []})

        return entities

    def step(self, time, inputs, max_advance=36000):
        '''
        Perform simulaation step

        :param time: Current simulation time
        :param inputs: Inputs as given by the simmulator
        :param max_advance: Max advance in seconds
        '''
        # Perform simulation step
        self.cache = {}

        for eid, attrs in inputs.items():
            for attr, values in attrs.items():
                # set value
                if attr == "Q_demand_signal":
                    q_demand_signal = sum(values.values())
                elif attr == "T_in_tank":
                    t_tank_input = sum(values.values())
                elif attr == "T_mean_tank":
                    t_mean_tank = sum(values.values())

        #===================================================================
        # Controller logic
        #===================================================================
        Q_Demand_controlled = 0

        # Turn off if water is too cold at the input
        if t_tank_input < self.min_temperature:
            Q_Demand_controlled = 0

        # Turn off if water is too hot at the output
        elif t_tank_input > self.max_input_temperature:
            Q_Demand_controlled = 0
        else:
            # If the tank is cold, force a heating step
            if t_mean_tank < self.min_tank_mean_T:

                # Choose the bigger of the values between the demand and the
                # minimum
                if self.Q_demand_min < q_demand_signal:
                    Q_Demand_controlled = q_demand_signal

                else:
                    Q_Demand_controlled = self.Q_demand_min

            # If the tank is hot enough, heat only the amount extracted
            else:
                if self.Q_demand_min < q_demand_signal:
                    Q_Demand_controlled = q_demand_signal

                else:
                    Q_Demand_controlled = 0

        #======================================================================
        # End of Logic
        #======================================================================

        self.cache = {}
        for eid in self._entities.keys():
            self.cache[eid] = Q_Demand_controlled

        # Next timestamp for simulation
        return time + self.step_size

    def get_data(self, outputs):
        '''
        This shall return none

        :param outputs: Dict with expected outputs.
        '''
        # create output data
        data = {}

        # loop over all output values
        for ename, attrs in outputs.items():
            # get model entry
            # set data entry for model
            data[ename] = {}

            # loop over all attributes
            for attr in attrs:
                if attr not in self.meta['models']["HeatpumpController"]['attrs']:
                    raise ValueError('Unknown output attribute: %s' % attr)

                # Get model data
                else:
                    if attr == "Q_Demand_controlled":
                        data[ename][attr] = float(self.cache[ename])

        return data


if __name__ == "__main__":
    pass
