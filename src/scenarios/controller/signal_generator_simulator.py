'''
Created on 08.11.2021

@author: Fernando Penaherrera @UOL/OFFIS

A simulator class for the signal generator

The input is a dict with date times and status information

The output is a signal True/False to be read by a further
simulator

This emulates the behavior of another entity.
'''
import mosaik_api
import itertools
from src.scenarios.controller.signal_generator import SignalGenerator


# Simulation Metadata
META = {
    'type': 'time-based',
    'models': {
        'Signal': {
            'public': True,
            'params': ["model_params"],
            'attrs': [
                "statusSim",         # Double value
            ],
        },
    },
    "extra_methods": ["get_entities"]}


class SignalSimulator(mosaik_api.Simulator):
    '''
    Mock-Up simulator that generates a :double: signal
    '''

    def __init__(self):
        '''
        Initialization
        All methods in the inheritance chain are called
        '''
        super(SignalSimulator, self).__init__(META)
        self.sid = None
        self.entities = {}
        self._entities = {}
        self.eid_counters = {}

    def init(self, sid, time_resolution=1, start_date=None, step_size=60):
        '''
        Initialization of entities

        :param sid: String ID.
        :param time_resolution: Resolution in seconds
        :param step_size: Step size in seconds
        '''

        # assign properties
        self.sid = sid
        self.time_resolution = time_resolution
        self.start_date = start_date
        self.step_size = step_size

        # return meta data
        return self.meta

    def create(self, num, model_type, model_params):
        '''

        :param num: Number of instances of the model_type
        :param model_type: Class specifiying the model_type. In this case, SignalGenerator
        :param start_date: Start Date. String format "YYYY-MM-DD HH:MM:SS"
        :param model_params: Dictionary for the initialization.
                             signals = {
                                        "2021-01-01 07:00:00": 1000,
                                        "2021-01-02 16:00:00": 600,
                                    }
        '''
        # Counter for IDs
        counter = self.eid_counters.setdefault(model_type, itertools.count())

        entities = []
        if model_params:
            self.model_params = model_params

        for _ in range(num):
            eid = '{}_{}'.format(model_type, next(counter))  # Entities IDs

            self._entities[eid] = SignalGenerator(
                start_date=self.start_date, signals=model_params)

            full_id = '{}.{}'.format(self.sid, model_type)
            entities.append({'eid': eid, 'type': model_type, 'rel': []})

            self.entities[eid] = {
                'ename': eid,
                'etype': model_type,
                'model': self._entities[eid],
                'full_id': full_id}

        return entities

    def step(self, time, inputs, max_advance=36000):
        '''
        Perform simulaation step

        :param time: Current simulation time
        :param inputs: Inputs as given by the simmulator
        :param max_advance: Max advance in seconds
        '''
        # Perform simulation step
        self.cache = {}
        for eid in self._entities.keys():
            self._entities[eid].step(self.step_size)
            self.cache[eid] = self._entities[eid].statusSim
        # Next timestamp for simulation
        return time + self.step_size

    def get_data(self, outputs):
        '''
        Get data of the model after the step()

        :param outputs: Output parameters

        Desired output parameters:
        "status"         # True/False value
        '''
        # create output data
        data = {}

        # loop over all output values
        for ename, attrs in outputs.items():
            # get model entry
            # set data entry for model
            data[ename] = {}

            # loop over all attributes
            for attr in attrs:
                if attr not in self.meta['models']["Signal"]['attrs']:
                    raise ValueError('Unknown output attribute: %s' % attr)

                # Get model data
                else:
                    if attr == "statusSim":

                        data[ename][attr] = float(self.cache[ename])

        # return data to mosaik

        return data

    def get_entities(self):
        # return entities of API
        return self.entities


if __name__ == "__main__":
    pass
