'''
Created on 08.06.2021

@author: Fernando Penaherrera @UOL/OFFIS

Common functions and parameters, such as directory names
'''

import os
from os.path import join


def get_project_root():
    """
    Returns the path to the project root directory.

    :return: Base directory path.
    :rtype: str
    """
    return os.path.realpath(os.path.join(
        os.path.dirname(__file__),
        os.pardir,
        os.pardir
    ))


BASE_DIR = get_project_root()
DATA_DIR = join(BASE_DIR, "data")
DATA_RAW_DIR = join(DATA_DIR, "raw")
SRC_DIR = join(BASE_DIR, "src")
RESULTS_DIR = join(BASE_DIR, "results")
TEST_RESULTS_DIR = join(RESULTS_DIR,"tests_results")
if __name__ == '__main__':
    pass