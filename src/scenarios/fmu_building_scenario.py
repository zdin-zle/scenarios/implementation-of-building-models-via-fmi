"""This scenario contains a building simulation simulated with Dymola (FMU Export)"""

# Import packages needed for the scenario.
from os.path import join

import mosaik
from src.scenarios.common import DATA_DIR, BASE_DIR

# Specify simulator configurations
sim_config = {
    'CSV': {
        'python': 'mosaik_csv:CSV',
    },
    'Collector': {
        'python': 'collector.collector_simulator:Collector'
    },
    'FMI': {
        'python': 'mosaik_fmi.mosaik_fmi:FmuAdapter',
    },
}

# Set scenario parameters:
END = 24 * 60 * 60 * 7 # 7 days in seconds
START = '2019-01-01 00:00:00'
METEO_DATA = join(DATA_DIR, 'processed/BS-MeteoData.csv')
SOLAR_DATA = join(DATA_DIR, 'processed/BS-SolarData60min.csv')

# Set up the "world" of the scenario
world = mosaik.World(sim_config, time_resolution=1.0)

# Initialize the simulators
meteo_data_sim = world.start('CSV', sim_start=START, datafile=METEO_DATA)
solar_data_sim = world.start('CSV', sim_start=START, datafile=SOLAR_DATA)

# Initialize the FMU Simulator
fmi_sim = world.start('FMI',
                     work_dir=join(BASE_DIR, 'src/scenarios/FMUs/'),
                     fmu_name='TL_0FMU_0V03',
                     model_name='TL_0FMU_0V03',
                     fmi_version='2',
                     fmi_type='cs',
                     logging_on=False,
                     instance_name='TL_0FMU_0V03',
                     step_size=60*60)

# Instantiate model entities
meteo_data = meteo_data_sim.Meteo.create(1)
solar_data = solar_data_sim.Solar.create(1)

fmi = fmi_sim.TL_0FMU_0V03.create(1,
                                 Heizung1=1.0,          #  ?
                                 Heizung2=2.0,          #  ?
                                 pAtm_in=1022.0,        # pressure at station height [hPA]
                                 tDewPoi_in=5.6,        # dew point temperature [C]
                                 tBlaSky_in=1.0,        # black body sky temperature [C]
                                 tDryBul_in=7.2,        # dry bulb temperature [C]
                                 relHum_in=90.0,        # relative humidity [%]
                                 winSpe_in=1.0,         # wind speed [m/s]
                                 winDir_in=1.0,         # wind direction [Deg]
                                 HInfHor_in=0.0,        # infrared horizontal radiation [W/m2]
                                 ceiHei_in=2000.0,      # ceiling height [m]
                                 totSkyCov_in=2.0,      # total sky cover [%]
                                 opaSkyCov_in=2.0,      # opaque sky cover [%]
                                 )

# Connect model entities
# TODO: Connect missing climate data with FMU input parameters
world.connect(meteo_data[0], fmi[0],
              ('WindSpeed', 'winSpe_in'),
              ('WindDir', 'winDir_in'))

world.connect(solar_data[0], fmi[0],
              ('DNI', 'HInfHor_in'))

# Some data collection
collector = world.start('Collector', start_date=START)
monitor = collector.Monitor()

# Saved in results/data/fmu_results.csv
world.connect(fmi[0], monitor,'TAir_zone1', 'TAir_zone2')

world.run(until=END, print_progress=True)
