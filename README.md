# Implementation of building models via FMI

### Contact person: 
Tobias Lege @HSOF

## Scenario description: 
In the scenario building model, a model of a multi-family building is created with the software Dymola and linked together with other Python models for the plant technology in the co-simulation platform Mosaik. The basis for the building model is the AixLib-library of the RWTH Aachen. The integration into the co-simulation is done via a FMI-port. The goal is to simulate load profiles for one year, plot the results in graphs and check them for plausibility. Furthermore, necessary interfaces in the co-simulation are to be tested, optimized and standardized.

## Motivation: 
*	Integration of building models in the co-simulation
*	Implementation of an FMI-port 
*	Modeling of regenerative supply scenarios
*	Determination and optimization of CO2-emissions

## Goal of the scenario:
*	A building model will be coupled to the co-simulation platform mosaic via FMI (functional mock-up interface)
*	Weather data will be forwarded to the building model via mosaic
*	The heating requirement is calculated by a heat balance
*	As a heating supply system a heat pump will be modeled and implemented
*	The courses of temperature as well as power and heat consumption are presented throughout the year at defined time steps. 
*	The CO2-emission due to heat pump operation are calculated each time step via emission factors and returned to the co-simulation

## Graphical Description:

![](docs/DSZ_building.JPG)  
_Figure 1: Graphical description._

![](docs/Energy_concept.jpg)   
_Figure 2: Energy concept._

## Installation

### Requirements for mosaik-fmi
* mosaik-fmi is based on the FMI++ library, which can be found at https://fmipp.sourceforge.io/
* The FMI++ python interface is used, which can be found at https://github.com/AIT-IES/py-fmipp. See this page also for details about installation and requirements of the python interface.

**Make sure to have installed the following prerequisites** \
(e.g. via apt-get, see package names in brackets below):

* python (`python-dev`) (recommended: version 3.5 (or higher))
* pip (`python-pip`)
* distutils (`python-setuptools`)
* GCC compiler toolchain (`build-essential`)
* swig (`swig`)
* SUNDIALS library (`libsundials-serial-dev`)
* Boost library (`libboost-all-dev`)

### Install packages via pip 
Make sure you install the requirements within a virtual environment \
(e.g., under Ubuntu with [virtualenv](https://virtualenv.pypa.io/en/latest/)):
    
    $ sudo pip install virtualenv
    $ virtualenv -p /usr/bin/python3 ~/.virtualenvs/mosaik-fmi
    $ source ~/.virtualenvs/mosaik-fmi/bin/activate

Then install requirements via pip:

    $ (mosaik-fmi) pip install -r REQUIREMENTS.txt

## Run: FMU building scenario
    $ (mosaik-fmi) python src/scenarios/fmu_building_scenario.py


