'''
Created on 09.11.2021

@author: Fernando Penaherrera @UOL/OFFIS
'''


from os.path import join
from src.scenarios.common import TEST_RESULTS_DIR
import mosaik
import h5py
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

SIM_CONFIG = {
    'HotWaterTankSim': {
        'python': 'mosaik_heatpump.hotwatertanksim.hotwatertank_mosaik:HotWaterTankSimulator',
    },
    'SignalGeneratorSim': {
        'python': 'src.scenarios.controller.signal_generator_simulator:SignalSimulator',
    },
    'TankControllerSim': {
        'python': 'src.scenarios.controller.tank_controller_simulator:TankControllerSimulator',
    },
    'CSV': {
        'python': 'mosaik_csv:CSV',
    },
    'DB': {
        'python': 'mosaik_hdf5:MosaikHdf5'},
}

# Simulation Configuration Parameters
DAY = 24 * 60 * 60
START = '2020-01-01 00:00:00'
END = 7 * DAY   # 2.5 Hours or 150 mins
STEP_SIZE = 30 * 60
DATA_BASE = join(TEST_RESULTS_DIR, 'control_tank_restults.hdf5')

# Component Sizing
tank_params = {
    'height': 2100,
    'diameter': 1200,
    'T_env': 14.0,
    'htc_walls': 1.0,
    'htc_layers': 20,
    'n_layers': 5,
    'n_sensors': 5,
    'heating_rods': {
        'hr_1': {
            'pos': 1800,
            'P_th_stages': [0, 500, 1000, 2000, 3000],
            "T_max": 90,
        }
    }
}

tank_init_vals = {
    'layers': {'T': [20, 25, 30, 35, 40]},
    'hr_1': { 'P_th_set': 0}
               }


signals = {
    "2020-01-01 07:00:00": 100,
    "2020-01-01 19:00:00": 200,
    "2020-01-02 02:00:00": 300,
    "2020-01-02 16:00:00": 200,
    "2020-01-02 20:00:00": 50,
    "2020-01-03 02:00:00": 300,
    "2020-01-04 16:00:00": 0,
    "2020-01-05 20:00:00": 50,
    "2020-01-06 02:00:00": 0,
    "2020-01-07 16:00:00": 100,

}


def create_world(SIM_CONFIG):
    world = mosaik.World(SIM_CONFIG)
    return world


def configure_scenario(world):
    # Configure the simulators
    hwt_Sim = world.start(
        'HotWaterTankSim',
        step_size=STEP_SIZE,
        config=tank_params)
    signal_Sim = world.start(
        "SignalGeneratorSim",
        step_size=STEP_SIZE,
        start_date=START)
    tank_ctrl_Sim = world.start('TankControllerSim', step_size=STEP_SIZE)
    db_Sim = world.start('DB', step_size=STEP_SIZE, duration=END)

    # Instantiate models

    tank_Model = hwt_Sim.HotWaterTank(
        params=tank_params, init_vals=tank_init_vals)
    signal_Model = signal_Sim.Signal.create(1, model_params=signals)

    tank_ctrl_Model = tank_ctrl_Sim.TankController.create(
        1, controlled_entities=tank_Model)
    db_Model = db_Sim.Database(filename=DATA_BASE)

    world.connect(signal_Model[0], tank_ctrl_Model[0], ("statusSim", "signal")
                  )

    # Air source for the heat pump
    tank_attrs = tank_Model.sim.meta["models"]["HotWaterTank"]["attrs"]
    #signal_attrs = signal_Model[0].sim.meta["models"]["Signal"]["attrs"]

    for attr in tank_attrs:
        if attr not in [
            "_",
            'sh_supply',
            'sh_demand',
            "dhw_demand",
            'dhw_supply',
            "hp_demand",
                "snapshot"]:
            world.connect(tank_Model, db_Model, attr)

    return world


def run_simulation(world):
    world.run(until=END, print_progress=False)


def analize_database(DATA_BASE):
    data = h5py.File(DATA_BASE, "r")
    tank_data = data["Series"]["HotWaterTankSim-0.HotWaterTank_0"]

    sensor_list = [k for k in tank_data.keys() if "sensor" in k]
    resulst_temp_tank = {k: np.array(tank_data[k]) for k in sensor_list}
    dti = pd.date_range(
        START,
        periods=END / STEP_SIZE,
        freq="{}s".format(STEP_SIZE))

    resulst_temp_tank["Date"] = dti
    df = pd.DataFrame(resulst_temp_tank)
    df.set_index("Date", inplace=True)

    # Write a graph
    ax1 = df.plot()
    ax1.set_title("Co-Simulation Results")
    ax1.set_ylabel('Temperatures')
    plt.savefig(join(TEST_RESULTS_DIR, "Tank_Controller_Simulation.jpg"))
       

def main():
    world = create_world(SIM_CONFIG)
    world = configure_scenario(world)
    run_simulation(world)
    analize_database(DATA_BASE)

if __name__ == '__main__':
    main()

