'''
Created on 01.11.2021

@author: Fernando Penaherrera @UOL/OFFIS
'''
import mosaik.util
import h5py
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from src.scenarios.common import DATA_RAW_DIR, TEST_RESULTS_DIR
from os.path import join
import logging

# Sim config. and other parameters
SIM_CONFIG = {
    'RadiatorSim': {
        'python': 'src.scenarios.radiator.radiator_simulator:RadiatorSimulator',
    },
    'CSV': {
        'python': 'mosaik_csv:CSV',
    },
    'DB': {
        'python': 'mosaik_hdf5:MosaikHdf5'},
}

day = 24 * 60 * 60
START = '2020-01-01 00:00:00'
END = 7 * day
RADIATOR_HEAT_DATA = join(DATA_RAW_DIR, "heating_radiator.csv")
DATA_BASE = join(TEST_RESULTS_DIR, 'radiator_results.hdf5')

STEP_SIZE = 15 * 60

radiator_params = {"t_supply_design": 70,
                   "t_return_design": 30,
                   "t_indoor_design": 22,
                   "method": "LMTD",
                   "Q_design": 15000,
                   "n_coeff": 1.3}


def cosimulation():
    # Create World
    world = mosaik.World(SIM_CONFIG)

    # Start simulators
    radiatorSim = world.start('RadiatorSim', step_size=STEP_SIZE)
    heatLoadSim = world.start(
        'CSV',
        sim_start=START,
        datafile=RADIATOR_HEAT_DATA)

    # Instantiate models
    radiatorModel = radiatorSim.Radiator.create(
        1, model_params=radiator_params)
    heatLoadModel = heatLoadSim.Heating.create(1)

    # Connect entities
    world.connect(
        heatLoadModel[0],
        radiatorModel[0],
        ('HouseHeating', 'Q_demand'),
        ('temp_in', 't_supply'),
        ("temp_indoor", "t_indoor"))

    # Initializing and instantiating a database component:
    db = world.start('DB', step_size=STEP_SIZE, duration=END)
    hdf5 = db.Database(filename=DATA_BASE)

    world.connect(
        radiatorModel[0],
        hdf5,
        "t_supply",         # Radiator supply temperature [C]
        "t_return",         # Radiator return temperature [C]
        "t_indoor",         # Indoor temperature [C]
        "Q_demand",         # Heat Demand [W]
        "massflow_in",)

    # Run simulation
    world.run(until=END, print_progress=False)


def results_analysis():
    '''
    Reads the data stored in the HDF5 database,
    converts to a Data Frame and plot the results,
    and saves the results to a csv.
    '''

    data = h5py.File(DATA_BASE, "r")
    series_set = data["Series"]["RadiatorSim-0.Radiator_0"]
    pars = ["Q_demand", "massflow_in", "t_indoor", "t_return", "t_supply"]

    data = {par: np.array(series_set[par]) for par in pars}

    data["massflow_in"] = data["massflow_in"] * 100000 # Scaling

    dti = pd.date_range(
        START,
        periods=len(data[pars[0]]),
        freq="{}s".format(STEP_SIZE))

    data["Date"] = dti

    df = pd.DataFrame(data)
    df.set_index("Date", inplace=True)

    fig, axs = plt.subplots(1, 2, figsize=(14, 7))

    fig.suptitle("Results")

    # Write a graph
    df[["Q_demand", "massflow_in"]].plot(ax=axs[0])
    df[["t_indoor", "t_return", "t_supply"]].plot(ax=axs[1])
    axs[0].set_ylabel('Heatflows')
    axs[1].set_ylabel('Temperatures')

    fig.savefig(join(TEST_RESULTS_DIR, "Radiator.jpg"))
    plt.show()
    logging.info("Saved results")


def main():
    cosimulation()
    results_analysis()


if __name__ == '__main__':
    main()
    pass
