'''
Created on 29.10.2021

@author: Fernando Penaherrera @UOL/OFFIS

Integration of tank

'''

import mosaik
import inspect
import sys
import os
from src.scenarios.common import DATA_RAW_DIR
from os.path import join


SIM_CONFIG = {
    'HotWaterTankSim': {
        'python': 'mosaik_heatpump.hotwatertanksim.hotwatertank_mosaik:HotWaterTankSimulator',
    },
    'CSV': {
        'python': 'mosaik_csv:CSV',
    },
    'DB': {
        'python': 'mosaik_hdf5:MosaikHdf5'},
}

START = '01.01.2016 00:00'
END = 7 * 15 * 60
HEAT_LOAD_DATA = join(DATA_RAW_DIR, 'heat_pump_model_data.csv')
date_format = 'DD.MM.YYYY HH:mm'

params = {
    'height': 2100,
    'diameter': 1200,
    'T_env': 20.0,
    'htc_walls': 1.0,
    'htc_layers': 20,
    'n_layers': 3,
    'n_sensors': 3,
    'connections': {
        'cc_in': {'pos': 1500},
        'cc_out': {'pos': 10},
        'hp_in': {'pos': 2000},
        'hp_out': {'pos': 100},
        },
    'heating_rods': {
            'hr_1': {
                'pos': 1800,
                'P_th_stages': [0, 500, 1000, 2000, 3000],
                "T_max": 80,
            }
        }
    }


world = mosaik.World(SIM_CONFIG)

# configure the simulators
hwtsim = world.start('HotWaterTankSim', step_size=15 * 60, config=params)

csv = world.start(
    'CSV',
    sim_start=START,
    datafile=HEAT_LOAD_DATA,
    date_format=date_format)


init_vals = {
    'layers': {'T': [30, 50, 70]},
    'hr_1': { 'P_el': 1000}
    
}

# Instantiate models
hwt = hwtsim.HotWaterTank(params=params, init_vals=init_vals)
csv_data = csv.HWT()

# Connect entities
world.connect(csv_data, hwt,
              ('T_in', 'cc_in.T'),
              ('T_in_hp', 'hp_in.T'),
              ('F_in', 'cc_in.F'),
              ('F_out', 'cc_out.F'),
              ('F_in_hp', 'hp_in.F'),
              ('F_out_hp', 'hp_out.F'))

# storage
db = world.start('DB', step_size=15 * 60, duration=END)
hdf5 = db.Database(filename='hwt_trial_4.hdf5')
world.connect(
    hwt,
    hdf5,
    'cc_in.T',
    'cc_in.F',
    'cc_out.T',
    'cc_out.F',
    'hp_in.T',
    'hp_in.F',
    'hp_out.T',
    'hp_out.F',
    'sensor_00.T',
    'sensor_01.T',
    'sensor_02.T',
    'T_mean')


world.run(until=END, print_progress=False)  # As fast as possilbe


if __name__ == '__main__':
    pass
