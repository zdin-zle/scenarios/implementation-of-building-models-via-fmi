'''
Created on 29.10.2021

@author: Fernando Penaherrera @UOL/OFFIS

intallation requirements (see also requirements.txt)
>>> pip install mosaik, mosaik-heatpump, mosaik-csv, mosaik-hdf5, matplotlib
pip install mosaik, mosaik-csv, mosaik-hdf5, matplotlib
'''
import mosaik
import h5py
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from src.scenarios.common import DATA_RAW_DIR, TEST_RESULTS_DIR
from os.path import join
import logging

# Sim config. and other parameters
SIM_CONFIG = {
    'HeatPumpSim': {
        'python': 'mosaik_heatpump.heatpump.Heat_Pump_mosaik:HeatPumpSimulator',
    },
    'CSV': {
        'python': 'mosaik_csv:CSV',
    },
    'DB': {
        'python': 'mosaik_hdf5:MosaikHdf5'
    },
}

day = 24 * 60 * 60
START = '2020-01-01 00:00:00'
END = 7 * day   # 2.5 Hours or 150 mins
HEAT_LOAD_DATA = join(DATA_RAW_DIR, "heating.csv")
TEMPERATURE_DATA = join(DATA_RAW_DIR, "temperature.csv")
DATA_BASE = join(TEST_RESULTS_DIR, 'heat_pump_results.hdf5')
STEP_SIZE = 60 * 60

heat_pump_params = {'heat_source': 'air',
                    #'cons_T': 40,
                    'cond_in_T': 25,
                    'calc_mode': 'fast',
                    }


def cosimulation():
    # Create World
    world = mosaik.World(SIM_CONFIG)

    # Start simulators
    heatpumpSim = world.start('HeatPumpSim', step_size=STEP_SIZE)
    heatLoadSim = world.start(
        'CSV',
        sim_start=START,
        datafile=HEAT_LOAD_DATA)
    temperatureSim = world.start(
        'CSV',
        sim_start=START,
        datafile=TEMPERATURE_DATA)

    # Instantiate models
    heatpumpModel = heatpumpSim.HeatPump.create(1, params=heat_pump_params)
    heatLoadModel = heatLoadSim.Heating.create(1)
    temperatureModel = temperatureSim.Temperature.create(1)

    # Connect entities
    world.connect(
        heatLoadModel[0],
        heatpumpModel[0],
        ('HouseHeating2',
         'Q_Demand'))

    world.connect(
        temperatureModel[0],
        heatpumpModel[0],
        ('AirTemp1',
         'heat_source_T'))

    # Initializing and instantiating a database component:
    db = world.start('DB', step_size=STEP_SIZE, duration=END)
    print(DATA_BASE)
    hdf5 = db.Database(filename=DATA_BASE)

    world.connect(
        heatpumpModel[0],
        hdf5,
        'Q_Demand',
        'Q_Supplied',
        'heat_source_T',
        'P_Required',
        'cons_T',
        'cond_m_in',
        'cond_m_out',
        'COP')

    world.connect(
        temperatureModel[0],
        hdf5,
        'AirTemp1')

    # Run simulation
    world.run(until=END)


def results_analysis():
    '''
    Reads the data stored in the HDF5 database,
    converts to a Data Frame and plot the results,
    and saves the results to a csv.
    '''

    data = h5py.File(DATA_BASE, "r")
    cop = data["Series"]["HeatPumpSim-0.HeatPump_0"]["COP"]
    q_demand = data["Series"]["HeatPumpSim-0.HeatPump_0"]["Q_Demand"]
    q_supplied = data["Series"]["HeatPumpSim-0.HeatPump_0"]["Q_Supplied"]
    p_required = data["Series"]["HeatPumpSim-0.HeatPump_0"]["P_Required"]
    print("COP", sum(cop) / len(cop))
    if sum(cop) > 0:
        dti = pd.date_range(
            START,
            periods=len(
                np.array(cop)),
            freq="{}s".format(STEP_SIZE))
        results = {
            "Date": dti,
            "COP": np.array(cop),
            "Q_Demand": np.array(q_demand),
            "Q_Supplied": np.array(q_supplied),
            "P_Required": np.array(p_required)
        }
        df = pd.DataFrame(results)
        df.set_index("Date", inplace=True)

        # Write a graph
        ax1 = df[["Q_Demand", "Q_Supplied", "P_Required"]].plot()
        ax2 = df["COP"].plot(secondary_y=True, style="y--")
        ax1.set_title("Co-Simulation Results")
        ax1.set_ylabel('Power Flows')
        ax2.set_ylabel('COP')
        plt.savefig(join(TEST_RESULTS_DIR, "CoSumulation.jpg"))

    else:
        logging.info("Results not working")


def main():
    cosimulation()
    results_analysis()


if __name__ == '__main__':
    main()
