'''
Created on 29.10.2021

@author: Fernando Penaherrera @UOL/OFFIS
'''
from os.path import join
from src.scenarios.common import DATA_RAW_DIR, TEST_RESULTS_DIR
import mosaik
import h5py
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os


SIM_CONFIG = {
    'HotWaterTankSim': {
        'python': 'mosaik_heatpump.hotwatertanksim.hotwatertank_mosaik:HotWaterTankSimulator',
    },
    'HeatPumpSim': {
        'python': 'mosaik_heatpump.heatpump.Heat_Pump_mosaik:HeatPumpSimulator',
    },
    'RadiatorSim': {
        'python': 'src.scenarios.radiator.radiator_simulator:RadiatorSimulator',
    },
    'SignalGeneratorSim': {
        'python': 'src.scenarios.controller.signal_generator_simulator:SignalSimulator',
    },
    'HeatpumpControllerSim': {
        'python': 'src.scenarios.controller.heatpump_controller_simulator:HeatpumpControllerSimulator',
    },
    'TankControllerSim': {
        'python': 'src.scenarios.controller.tank_controller_simulator:TankControllerSimulator',
    },
    'CSV': {
        'python': 'mosaik_csv:CSV',
    },
    'DB': {
        'python': 'mosaik_hdf5:MosaikHdf5'},
}

# Simulation Configuration Parameters
DAY = 24 * 60 * 60
START = '2020-01-01 00:00:00'
END = 7 * DAY
STEP_SIZE = 10 * 60

TEST_RESULTS_DIR = os.path.join(TEST_RESULTS_DIR, "ControlledNetwork")
if not os.path.isdir(TEST_RESULTS_DIR):
    os.mkdir(TEST_RESULTS_DIR)

HEAT_LOAD_DATA = join(DATA_RAW_DIR, "heating_radiator.csv")
TEMPERATURE_DATA = join(DATA_RAW_DIR, "temperature.csv")
DATA_BASE = join(TEST_RESULTS_DIR, 'heat_network_controlled_results.hdf5')

# Component Sizing
heatpump_params = {'heat_source': 'air',
                   'calc_mode': 'fast',
                   }

radiator_params = {"t_supply_design": 40,
                   "t_return_design": 20,
                   "t_indoor_design": 18,
                   "method": "LMTD",
                   "Q_design": 10000,
                   "n_coeff": 1.3
                   }

tank_params = {
    'height': 2100,
    'diameter': 1200,
    'T_env': 14.0,
    'htc_walls': 1.0,
    'htc_layers': 20,
    'n_layers': 5,
    'n_sensors': 5,
    'connections': {
        'cc_in': {'pos': 10},
        'cc_out': {'pos': 1900},
        'hp_in': {'pos': 1800},
        'hp_out': {'pos': 1200},
    },
    'heating_rods': {
        'hr_1': {
            'pos': 1800,
            'P_th_stages': [0, 1000, 2000, 3000, 4000, 5000],
            "T_max": 80,
        }
    }
}


tank_init_vals = {
    'layers': {'T': [20, 25, 30, 35, 40]},
    'hr_1': {'P_th_set': 0}
}

heatpump_controller_params = {
    "min_temperature": 25,
    "max_input_temperature": 50,
    "min_tank_mean_T": 40,
    "Q_demand_min": 5000,
}

# Mock up for the signals that may come from a flexibility agent.
signals = {
    "2020-01-01 07:00:00": 1000,
    "2020-01-01 19:00:00": 2000,
    "2020-01-02 02:00:00": 3000,
    "2020-01-02 16:00:00": 2000,
    "2020-01-02 20:00:00": 5000,
    "2020-01-03 02:00:00": 3000,
    "2020-01-04 00:00:00": 0,
    "2020-01-05 00:00:00": 5000,
    "2020-01-06 00:00:00": 0,
    "2020-01-07 00:00:00": 1000,
    "2020-01-07 12:00:00": 0,
}


def create_world(SIM_CONFIG):
    world = mosaik.World(SIM_CONFIG)
    return world


def configure_scenario(world):
    # Configure the simulators
    heatpump_Sim = world.start('HeatPumpSim', step_size=STEP_SIZE)
    tank_Sim = world.start(
        'HotWaterTankSim',
        step_size=STEP_SIZE,
        config=tank_params)
    radiator_Sim = world.start('RadiatorSim', step_size=STEP_SIZE)
    heat_load_Sim = world.start(
        'CSV',
        sim_start=START,
        datafile=HEAT_LOAD_DATA)
    temperature_Sim = world.start(
        'CSV',
        sim_start=START,
        datafile=TEMPERATURE_DATA)
    signal_Sim = world.start(
        "SignalGeneratorSim",
        step_size=STEP_SIZE,
        start_date=START)
    heatpump_ctrl_Sim = world.start(
        "HeatpumpControllerSim",
        step_size=STEP_SIZE,
        start_date=START)
    tank_ctrl_Sim = world.start(
        'TankControllerSim',
        # start_date=START,
        step_size=STEP_SIZE)
    db_Sim = world.start('DB', step_size=STEP_SIZE, duration=END)

    # Instantiate models
    heatpump_Model = heatpump_Sim.HeatPump.create(1, params=heatpump_params)
    heatpump_ctrl_Model = heatpump_ctrl_Sim.HeatpumpController.create(
        1, model_params=heatpump_controller_params)

    tank_Model = tank_Sim.HotWaterTank(
        params=tank_params, init_vals=tank_init_vals)

    tank_ctrl_Model = tank_ctrl_Sim.TankController.create(
        1, controlled_entities=tank_Model)

    radiator_Model = radiator_Sim.Radiator.create(
        1, model_params=radiator_params)
    heat_Load_Model = heat_load_Sim.Heating.create(1)
    temperature_Model = temperature_Sim.Temperature.create(1)
    signal_Model = signal_Sim.Signal.create(1, model_params=signals)
    db_Model = db_Sim.Database(filename=DATA_BASE)

    # Connect the Radiator with the heat demand
    world.connect(heat_Load_Model[0], radiator_Model[0],
                  ('HouseHeating', 'Q_demand'),
                  ("temp_indoor", "t_indoor"))

    # Connect the "external" mockup signal to the hot water tank
    world.connect(signal_Model[0], tank_ctrl_Model[0], ("statusSim", "signal"))

    # Connect the tank to the controller and back back to set the power
    world.connect(tank_Model, tank_ctrl_Model[0],
                  ("sensor_04.T", "T_upper_sensor"),
                  ("sensor_02.T", "T_heatpump_sensor"))
    world.connect(tank_ctrl_Model[0], tank_Model,
                  ("P_th_set", "hr_1.P_th_set"),
                  time_shifted=True,
                  initial_data={"P_th_set": 0})

    # Air source for the heat pump
    world.connect(
        temperature_Model[0],
        heatpump_Model[0],
        ('AirTemp1', 'heat_source_T'))

    # Connect the Radiator with the Hot Water Tank and back for return
    world.connect(tank_Model, radiator_Model[0],
                  ('sensor_04.T', 't_supply'))

    world.connect(radiator_Model[0], tank_Model,
                  ('t_supply', 'cc_out.T'),
                  ('t_return', 'cc_in.T'),
                  ('massflow_in', 'cc_in.F'),
                  ('massflow_out', 'cc_out.F'),
                  time_shifted=True,
                  initial_data={'t_supply': 50})

    # Connect the Heat Pump with the Hot Water Tank and back for return
    world.connect(tank_Model, heatpump_Model[0],
                  ('sensor_02.T', 'cond_in_T'))

    world.connect(heatpump_Model[0], tank_Model,
                  ('cond_in_T', 'hp_out.T'),
                  ('cons_T', 'hp_in.T'),
                  ('cond_m_out', 'hp_in.F'),
                  ('cond_m_in', 'hp_out.F'), time_shifted=True,
                  initial_data={'cond_in_T': 30})

    # Heatpump controller
    world.connect(heat_Load_Model[0],
                  heatpump_ctrl_Model[0],
                  ('HouseHeating', 'Q_demand_signal'))

    world.connect(tank_Model, heatpump_ctrl_Model[0],
                  ('sensor_02.T', "T_in_tank"),
                  ("T_mean", "T_mean_tank"))

    world.connect(heatpump_ctrl_Model[0], heatpump_Model[0],
                  ('Q_Demand_controlled', 'Q_Demand'))

    # Connect the Database
    heatpump_attrs = heatpump_Model[0].sim.meta["models"]["HeatPump"]["attrs"]
    radiator_attrs = radiator_Model[0].sim.meta["models"]["Radiator"]["attrs"]
    tank_attrs = tank_Model.sim.meta["models"]["HotWaterTank"]["attrs"]
    temperature_attrs = temperature_Model[0].sim.meta["models"]["Temperature"]["attrs"]
    heat_load_attrs = heat_Load_Model[0].sim.meta["models"]["Heating"]["attrs"]

    for attr in heatpump_attrs:
        world.connect(heatpump_Model[0], db_Model, attr)

    for attr in radiator_attrs:
        world.connect(radiator_Model[0], db_Model, attr)

    for attr in temperature_attrs:
        world.connect(temperature_Model[0], db_Model, attr)

    for attr in heat_load_attrs:
        world.connect(heat_Load_Model[0], db_Model, attr)

    for attr in tank_attrs:
        if attr not in [
            "_",
            'sh_supply',
            'sh_demand',
            "dhw_demand",
            'dhw_supply',
            "hp_demand",
                "snapshot"]:
            world.connect(tank_Model, db_Model, attr)

    return world


def run_simulation(world):
    world.run(until=END, print_progress=False)


def replace_0_with_nan(dataframe):
    for k, v in dataframe.items():
        dataframe[k] = [np.nan if x == 0 else x for x in v]

    return dataframe


def analize_database(DATA_BASE):
    data = h5py.File(DATA_BASE, "r")
    heatpump_data = data["Series"]["HeatPumpSim-0.HeatPump_0"]
    tank_data = data["Series"]["HotWaterTankSim-0.HotWaterTank_0"]
    radiator_data = data["Series"]["RadiatorSim-0.Radiator_0"]

    # Write a graph for tank sensor temperatures
    sensor_list = [k for k in tank_data.keys() if "sensor" in k]
    resulst_temp_tank = {k: np.array(tank_data[k]) for k in sensor_list}
    dti = pd.date_range(
        START,
        periods=END / STEP_SIZE,
        freq="{}s".format(STEP_SIZE))

    resulst_temp_tank["Date"] = dti
    df = pd.DataFrame(resulst_temp_tank)
    df.set_index("Date", inplace=True)
    ax1 = df.plot()
    ax1.set_title("Co-Simulation Results")
    ax1.set_ylabel('Temperatures')
    plt.savefig(join(TEST_RESULTS_DIR, "Heat_Network_Control_Tank_Temps.jpg"))

    # Write a graph for tank component inflow and outflow temperatures
    results_temp = {}
    results_temp["Heatpump Input"] = heatpump_data["cond_in_T"]
    results_temp["Heatpump Output"] = heatpump_data["cons_T"]
    results_temp["Radiator Input"] = radiator_data["t_supply"]
    results_temp["Radiator Output"] = radiator_data["t_return"]
    results_temp["Tank Mean"] = tank_data["T_mean"]
    results_temp["Date"] = dti

    results_temp = replace_0_with_nan(results_temp)
    df2 = pd.DataFrame(results_temp)
    df2.set_index("Date", inplace=True)
    ax2 = df2.plot()
    ax2.set_title("Co-Simulation Results")
    ax2.set_ylabel('Temperatures')
    plt.savefig(
        join(
            TEST_RESULTS_DIR,
            "Heat_Network_Control_Network_Temps.jpg"))

    # Write a graph for energy flows
    results_energy = {}
    heatpump_thermal_output = heatpump_data["Q_Supplied"]
    heatpump_electricity_input = heatpump_data["P_Required"]
    radiator_demand = radiator_data["Q_demand"]
    tank_electricity = tank_data["hr_1.P_el"]
    results_energy["Heatpump Out Th"] = heatpump_thermal_output
    results_energy["Heatpump In El"] = heatpump_electricity_input
    results_energy["Tank In El"] = tank_electricity
    results_energy["Radiator Out Th"] = radiator_demand
    results_energy["Date"] = dti
    results_energy = replace_0_with_nan(results_energy)
    df3 = pd.DataFrame(results_energy)
    df3.set_index("Date", inplace=True)
    ax3 = df3.plot()
    ax3.set_title("Co-Simulation Results")
    ax3.set_ylabel('EnergyFlows')
    plt.savefig(
        join(
            TEST_RESULTS_DIR,
            "Heat_Network_Control_Energy.jpg"))

    # General temperatures graph
    air_temp_data = data["Series"]["CSV-1.Temperature_0"]
    heat_load_data = data["Series"]["CSV-0.Heating_0"]
    results_gen = {}
    results_gen["Heat Pump Output"] = heatpump_data["cons_T"]
    results_gen["Tank Mean Temp"] = tank_data["T_mean"]
    results_gen["Air Temp"] = air_temp_data["AirTemp1"]
    results_gen["Indoor Temp"] = heat_load_data["temp_indoor"]
    results_gen["Radiator Input"] = radiator_data["t_supply"]
    results_gen["Date"] = dti
    results_gen = replace_0_with_nan(results_gen)

    df2 = pd.DataFrame(results_gen)
    df2.set_index("Date", inplace=True)
    ax2 = df2.plot()
    ax2.set_title("Co-Simulation Results")
    ax2.set_ylabel('Temperatures')
    plt.savefig(
        join(
            TEST_RESULTS_DIR,
            "Heat_Network_Control_Network_General_Temps.jpg"))

    path = os.path.realpath(TEST_RESULTS_DIR)
    os.startfile(path)


def main():
    world = create_world(SIM_CONFIG)
    world = configure_scenario(world)
    run_simulation(world)
    analize_database(DATA_BASE)


if __name__ == '__main__':
    main()
    pass
