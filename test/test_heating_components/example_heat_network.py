'''
Created on 29.10.2021

@author: Fernando Penaherrera @UOL/OFFIS
'''
from os.path import join
from src.scenarios.common import DATA_RAW_DIR, TEST_RESULTS_DIR
import mosaik
import h5py
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

SIM_CONFIG = {
    'HotWaterTankSim': {
        'python': 'mosaik_heatpump.hotwatertanksim.hotwatertank_mosaik:HotWaterTankSimulator',
    },
    'HeatPumpSim': {
        'python': 'mosaik_heatpump.heatpump.Heat_Pump_mosaik:HeatPumpSimulator',
    },
    'RadiatorSim': {
        'python': 'src.scenarios.radiator.radiator_simulator:RadiatorSimulator',
    },
    'SignalGeneratorSim': {
        'python': 'src.scenarios.controller.signal_generator_simulator:SignalSimulator',
    },
    'HeatpumpControllerSim': {
        'python': 'src.scenarios.controller.heatpump_controller_simulator:HeatpumpControllerSimulator',
    },
    'TankControllerSim': {
        'python': 'src.scenarios.controller.tank_controller_simulator:TankControllerSimulator',
    },
    'CSV': {
        'python': 'mosaik_csv:CSV',
    },
    'DB': {
        'python': 'mosaik_hdf5:MosaikHdf5'},
}

# Simulation Configuration Parameters
DAY = 24 * 60 * 60
START = '2020-01-01 00:00:00'
END = 7 * DAY   # 2.5 Hours or 150 mins
STEP_SIZE = 60 * 60

HEAT_LOAD_DATA = join(DATA_RAW_DIR, "heating_radiator.csv")
TEMPERATURE_DATA = join(DATA_RAW_DIR, "temperature.csv")
DATA_BASE = join(TEST_RESULTS_DIR, 'heat_network__controlled_results.hdf5')

# Component Sizing
heatpump_params = {'heat_source': 'air',
                   'calc_mode': 'fast',
                   }

radiator_params = {"t_supply_design": 40,
                   "t_return_design": 20,
                   "t_indoor_design": 18,
                   "method": "LMTD",
                   "Q_design": 10000,
                   "n_coeff": 1.3
                   }

tank_params = {
    'height': 2100,
    'diameter': 1200,
    'T_env': 14.0,
    'htc_walls': 1.0,
    'htc_layers': 20,
    'n_layers': 5,
    'n_sensors': 5,
    'connections': {
        'cc_in': {'pos': 10},
        'cc_out': {'pos': 1900},
        'hp_in': {'pos': 2000},
        'hp_out': {'pos': 1050},
    }
}

tank_init_vals = {
    'layers': {'T': [20, 40, 60, 70, 80]},
    'hr_1': {'P_th_set': 0}
}


signals = {
    "2020-01-01 07:00:00": 100,
    "2020-01-01 19:00:00": 200,
    "2020-01-02 02:00:00": 300,
    "2020-01-02 16:00:00": 200,
    "2020-01-02 20:00:00": 50,
    "2020-01-03 02:00:00": 300,
    "2020-01-04 16:00:00": 0,
    "2020-01-05 20:00:00": 50,
    "2020-01-06 02:00:00": 0,
    "2020-01-07 16:00:00": 100,

}


def create_world(SIM_CONFIG):
    world = mosaik.World(SIM_CONFIG)
    return world


def configure_scenario(world):
    # Configure the simulators
    heatpump_Sim = world.start('HeatPumpSim', step_size=STEP_SIZE)
    hwt_Sim = world.start(
        'HotWaterTankSim',
        step_size=STEP_SIZE,
        config=tank_params)
    radiator_Sim = world.start('RadiatorSim', step_size=STEP_SIZE)
    heat_load_Sim = world.start(
        'CSV',
        sim_start=START,
        datafile=HEAT_LOAD_DATA)
    temperature_Sim = world.start(
        'CSV',
        sim_start=START,
        datafile=TEMPERATURE_DATA)
    signal_Sim = world.start(
        "SignalGeneratorSim",
        step_size=STEP_SIZE,
        start_date=START)

    db_Sim = world.start('DB', step_size=STEP_SIZE, duration=END)

    # Instantiate models
    heatpump_Model = heatpump_Sim.HeatPump.create(1, params=heatpump_params)

    tank_Model = hwt_Sim.HotWaterTank(
        params=tank_params, init_vals=tank_init_vals)
    radiator_Model = radiator_Sim.Radiator.create(
        1, model_params=radiator_params)
    heat_Load_Model = heat_load_Sim.Heating.create(1)
    temperature_Model = temperature_Sim.Temperature.create(1)
    signal_Model = signal_Sim.Signal.create(1, model_params=signals)
    db_Model = db_Sim.Database(filename=DATA_BASE)

 
    # I will model the head demand to the heat pump as equal to the demand,
    # for now
    world.connect(heat_Load_Model[0],
                  heatpump_Model[0],
                  ('HouseHeating', 'Q_Demand'))



    # Connect the Radiator with the heat demand
    world.connect(heat_Load_Model[0], radiator_Model[0],
                  ('HouseHeating', 'Q_demand'),
                  ("temp_indoor", "t_indoor"))

    # Air source for the heat pump
    world.connect(
        temperature_Model[0],
        heatpump_Model[0],
        ('AirTemp1', 'heat_source_T'))

    # Connect the Radiator with the Hot Water Tank
    world.connect(tank_Model, radiator_Model[0],
                  ('sensor_04.T', 't_supply'))

    world.connect(radiator_Model[0], tank_Model,
                  ('t_supply', 'cc_out.T'),
                  ('t_return', 'cc_in.T'),
                  ('massflow_in', 'cc_in.F'),
                  ('massflow_out', 'cc_out.F'),
                  time_shifted=True,
                  initial_data={'t_supply': 50})
                  
    # Connect the Heat Pump with the Hot Water Tank


    world.connect(tank_Model, heatpump_Model[0],
                  ('sensor_02.T', 'cond_in_T'))
    world.connect(heatpump_Model[0], tank_Model,
                  ('cond_in_T', 'hp_out.T'),
                  ('cons_T', 'hp_in.T'),
                  ('cond_m_out', 'hp_in.F'),
                  ('cond_m_in', 'hp_out.F'), time_shifted=True,
                  initial_data={'cond_in_T': 30}
                  )
    # Connect the Database
    heatpump_attrs = heatpump_Model[0].sim.meta["models"]["HeatPump"]["attrs"]
    radiator_attrs = radiator_Model[0].sim.meta["models"]["Radiator"]["attrs"]
    tank_attrs = tank_Model.sim.meta["models"]["HotWaterTank"]["attrs"]

    for attr in heatpump_attrs:
        world.connect(heatpump_Model[0], db_Model, attr)

    for attr in radiator_attrs:
        world.connect(radiator_Model[0], db_Model, attr)

    for attr in tank_attrs:
        if attr not in [
            "_",
            'sh_supply',
            'sh_demand',
            "dhw_demand",
            'dhw_supply',
            "hp_demand",
                "snapshot"]:
            world.connect(tank_Model, db_Model, attr)

    return world


def run_simulation(world):
    world.run(until=END, print_progress=False)


def analize_database(DATA_BASE):
    data = h5py.File(DATA_BASE, "r")
    heatpump_data = data["Series"]["HeatPumpSim-0.HeatPump_0"]
    tank_data = data["Series"]["HotWaterTankSim-0.HotWaterTank_0"]
    radiator_data = data["Series"]["RadiatorSim-0.Radiator_0"]

    sensor_list = [k for k in tank_data.keys() if "sensor" in k]
    resulst_temp_tank = {k: np.array(tank_data[k]) for k in sensor_list}
    dti = pd.date_range(
        START,
        periods=END / STEP_SIZE,
        freq="{}s".format(STEP_SIZE))

    resulst_temp_tank["Date"] = dti
    df = pd.DataFrame(resulst_temp_tank)
    df.set_index("Date", inplace=True)

    # Write a graph
    ax1 = df.plot()
    ax1.set_title("Co-Simulation Results")
    ax1.set_ylabel('Temperatures')
    plt.savefig(join(TEST_RESULTS_DIR, "Heat_Network_Control_Tank_Temps.jpg"))

    results_temp = {}
    results_temp["Heatpump Input"] = heatpump_data["cond_in_T"]
    results_temp["Heatpump Output"] = heatpump_data["cons_T"]
    results_temp["Radiator Input"] = radiator_data["t_supply"]
    results_temp["Radiator Output"] = radiator_data["t_return"]
    results_temp["Tank Mean"] = tank_data["T_mean"]
    results_temp["Date"] = dti
    df2 = pd.DataFrame(results_temp)
    df2.set_index("Date", inplace=True)
    ax2 = df2.plot()
    ax2.set_title("Co-Simulation Results")
    ax2.set_ylabel('Temperatures')
    plt.savefig(
        join(
            TEST_RESULTS_DIR,
            "Heat_Network_Control_Network_Temps.jpg"))
    
    results_energy={}
    heatpump_thermal_output = heatpump_data["Q_Supplied"]
    heatpump_electricity_input = heatpump_data["P_Required"]
    radiator_demand = radiator_data["Q_demand"]
    tank_electricity = tank_data["Q_demand"]
    results_energy["Heatpump Out Th"] =heatpump_thermal_output
    results_energy["Heatpump In El"] = heatpump_electricity_input
    results_energy["Radiator Out Th"] =radiator_demand
    results_energy["Date"] = dti
    df3 = pd.DataFrame(results_energy)
    df3.set_index("Date", inplace=True)
    ax3 = df3.plot(style = "--")
    ax3.set_title("Co-Simulation Results")
    ax3.set_ylabel('EnergyFlows')
    plt.savefig(
        join(
            TEST_RESULTS_DIR,
            "Heat_Network_Control_Energy.jpg"))
    
    import os

    path = os.path.realpath(TEST_RESULTS_DIR)
    os.startfile(path)
    
def main():
    world = create_world(SIM_CONFIG)
    world = configure_scenario(world)
    run_simulation(world)
    analize_database(DATA_BASE)


if __name__ == '__main__':
    main()

